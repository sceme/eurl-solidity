const { upgradeProxy } = require('@openzeppelin/truffle-upgrades');

require('dotenv').config();
const proxy = process.env.PROXY_CONTRACT;
const owner = process.env.OWNER_GNOSIS;

const EURLTokenV2 = artifacts.require('EURLTokenV2');

module.exports = async function (deployer) {
  if(deployer.network === 'test') return;
  
  console.log("   EURL UPGRADE @", proxy, ":");
  console.log("   ---------------------------------------------------");

  const tokenV2 = await upgradeProxy(proxy, EURLTokenV2, { deployer });
  console.log("   > UPGRADED", tokenV2.address);

  await tokenV2.setOwner(owner);
  console.log("   > EURL OWNER SET: " + owner);
};