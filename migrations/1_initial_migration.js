const Migrations = artifacts.require("Migrations");

module.exports = async function (deployer) {
    if(deployer.network === 'test') return;
    deployer.deploy(Migrations);
};
