const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const keccak256 = require('keccak256');

require('dotenv').config();
const administrator = process.env.ADMIN_GNOSIS;
const masterMinter = process.env.MASTER_MINTER_GNOSIS;
const owner = process.env.OWNER_GNOSIS;

const Forwarder = artifacts.require('Forwarder');

module.exports = async function (deployer) {
    if(deployer.network === 'test') return;
    
    console.log("   EURL FORWARDER MIGRATION:");

    const forwarder = await deployProxy(Forwarder, [eurltoken.address]);
    await eurltoken.addTrustedForwarder(forwarder.address);
    const isForwarder = await eurltoken.isTrustedForwarder(forwarder.address);
    if (isForwarder) console.log("   > EURL FORWARDER SET: " + forwarder.address);
};