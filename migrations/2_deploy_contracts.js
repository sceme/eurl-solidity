const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const keccak256 = require('keccak256');

require('dotenv').config();
const administrator = process.env.ADMIN_GNOSIS;
const masterMinter = process.env.MASTER_MINTER_GNOSIS;
const owner = process.env.OWNER_GNOSIS;

const EURLToken = artifacts.require('EURLToken');
const Forwarder = artifacts.require('Forwarder');

module.exports = async function (deployer) {
    if(deployer.network === 'test') return;
    
    console.log("   EURL MIGRATION:");
    console.log("   ---------------");
    const eurltoken = await deployProxy(EURLToken, { kind: 'uups' }, {deployer});
    console.log("   > EURL PROXY DEPLOYED: " + eurltoken.address);

    await eurltoken.setAdministrator(administrator);
    const admin_address = await eurltoken.getRoleMember(keccak256('ADMIN'),0);
    console.log("   > EURL ADMIN SET: " + admin_address);

    //await eurltoken.setMasterMinter(masterMinter);
    //const minter_address = await eurltoken.getRoleMember(keccak256('MASTER_MINTER'),0);
    //console.log("   > EURL MASTER MINTER SET: " + minter_address);

    /*
    const forwarder = await deployProxy(Forwarder, [eurltoken.address]);
    await eurltoken.setTrustedForwarder(forwarder.address);
    const isForwarder = await eurltoken.isTrustedForwarder(forwarder.address);
    if (isForwarder) console.log("   > EURL FORWARDER SET: " + forwarder.address);
    */

    await eurltoken.setOwner(owner);
    await eurltoken.transferOwnership(owner);
    console.log("   > EURL OWNER SET: " + owner);
};