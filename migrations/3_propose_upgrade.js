const { prepareUpgrade } = require('@openzeppelin/truffle-upgrades');
const { AdminClient } = require('defender-admin-client');

require('dotenv').config();
const client = new AdminClient({apiKey: process.env.DEFENDER_TEAM_API_KEY, apiSecret: process.env.DEFENDER_TEAM_API_SECRET});
const EURLTokenV2 = artifacts.require('EURLTokenV2');
const proxy = process.env.PROXY_CONTRACT;
const gnosisSafe = process.env.OWNER_GNOSIS;

module.exports = async function (deployer, network) {
    if(deployer.network === 'test') return;
    
    console.log("   EURL UPGRADE PROPOSAL:");
    console.log("   ---------------------");

    const eurltoken2 = await prepareUpgrade(proxy, EURLTokenV2, {kind: 'uups'}, {deployer});
    console.log("   > NEW EURL PROPOSAL ADDRESS: ", eurltoken2);

    const newImplementation = await eurltoken2; 
    const newImplementationAbi = EURLTokenV2.abi;
    const via = gnosisSafe;
    const viaType = 'Gnosis Safe'; 
    
    const contract = { network: network, address: proxy}; 
    const proposal = await client.proposeUpgrade(
        {
            newImplementation, 
            newImplementationAbi, 
            via, 
            viaType
        }, 
        contract
    );
    console.log("   > NEW EURL PROPOSAL URL: ", proposal.url);
};