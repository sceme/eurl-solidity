const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const assert = require('assert');
const truffleAssert = require('truffle-assertions');
const sign = require('../utils/sign/metatx-sign');

/*const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));*/

const EURLToken = artifacts.require('EURLToken');
const Forwarder = artifacts.require('Forwarder');

const {Interface} = require('@ethersproject/abi');



contract('compile', (accounts) => {
    it("passes eurl meta-tx tests", async function () {

        const owner_dar = accounts[0];
        const administrator = accounts[1];
        const masterMinter = accounts[2];
        const reserve = accounts[3];
        const bob = accounts[4];
        await web3.eth.accounts.wallet.create(1)
        const aliceAccount = web3.eth.accounts.wallet[0];
        const alice = aliceAccount.address;
        const minter = accounts[5];
        const feesFaucet = accounts[6];

        console.log(alice);

        const eurltoken = await deployProxy(EURLToken);

        await eurltoken.setAdministrator(administrator, { from: owner_dar });
        await eurltoken.setMasterMinter(masterMinter, { from: owner_dar });

        await eurltoken.addMinter(minter, 10000,{ from: masterMinter });
        await eurltoken.mint(reserve, 1000, { from: minter });

        await eurltoken.transfer(alice, 200, { from: reserve });

        //case 31 - permit
        const value = 50;

        console.log("test1");

        const result = await sign.signERC2612Permit(web3.eth.currentProvider , eurltoken.address, aliceAccount, bob, value);

        await eurltoken.permit(alice, bob, value, result.deadline, 
            result.v, result.r, result.s, { from: bob });

        await eurltoken.transferFrom(alice, bob, value, { from: bob });

        assert.equal(await eurltoken.balanceOf(bob), 50, "Failed case31");
        assert.equal(await eurltoken.balanceOf(alice), 150, "Failed case31");
        console.log("case31 passed");

        //case 32 - set fee faucet address
        await eurltoken.setFeeFaucet(feesFaucet, { from: administrator });
        console.log("case32 passed");

        //case 33 - update gasless_basefee
        await eurltoken.updateGaslessBasefee(5, { from: administrator });
        assert.equal(await eurltoken.getGaslessBasefee(), 5, "Failed case33");
        console.log("case33 passed");

        //case 34 - update fee_rate
        const rate = 1000;
        await eurltoken.updateTxFeeRate(rate, { from: administrator });
        assert.equal(await eurltoken.getTxFeeRate(), rate, "Failed case34");
        console.log("case34 passed");

        //case 35 - transfer with fees
        await eurltoken.transfer(alice, 10, { from: bob });

        assert.equal(await eurltoken.balanceOf(bob), 39, "Failed case35");
        assert.equal(await eurltoken.balanceOf(alice), 160, "Failed case35");
        assert.equal(await eurltoken.balanceOf(feesFaucet), 1, "Failed case35");
        console.log("case35 passed");

        //case 36 - trustedForwarder
        const forwarder = await deployProxy(Forwarder, [eurltoken.address]);
        await eurltoken.setTrustedForwarder(forwarder.address, { from: administrator });

        assert.equal(await eurltoken.isTrustedForwarder(forwarder.address), true, "Failed case36");
        assert.equal(await forwarder.getEurl(), eurltoken.address, "Failed case36");
        console.log("case36 passed");

        //case 37 - bob forwards transaction where alice sends 10 EURL to bob signed by alice
        const { abi } = require('../build/contracts/EURLToken.json');
        const iEurl = await new Interface(abi);
        const data = iEurl.encodeFunctionData("transfer", [bob, 10]);

        const result2 = await sign.signForward(
          web3.eth.currentProvider, 
          1337, aliceAccount, 
          eurltoken.address, 
          forwarder.address, 
          1e6, 0, data
        );

        await truffleAssert.passes( forwarder.verify(
          result2.request, 
          result2.domainSeparator, 
          result2.TypeHash, 
          result2.suffixData, 
          result2.signature
        ), "Failed case37");
        
        await forwarder.execute(
          result2.request, 
          result2.domainSeparator, 
          result2.TypeHash, 
          result2.suffixData, 
          result2.signature, 
          {from: bob, gas: 1e6}
        );

        assert.equal(await eurltoken.balanceOf(bob), 54, "Failed case37");
        assert.equal(await eurltoken.balanceOf(alice), 144, "Failed case37");
        assert.equal(await eurltoken.balanceOf(feesFaucet), 2, "Failed case37");
        console.log("case37 passed");

        //case 38 - update incorrect fee_rate
        const rate2 = 100000;
        await truffleAssert.fails(eurltoken.updateTxFeeRate(rate2, { from: administrator }));
        console.log("case38 passed");

        //case 39 - Bob tries to call payGaslessBasefee from alice to bob
        await truffleAssert.fails(eurltoken.payGaslessBasefee(alice, bob, {from: bob}));
        console.log("case39 passed");

        //case 40 - update gasless_basefee (not enough funds)
        await eurltoken.updateGaslessBasefee(500, { from: administrator });
        assert.equal(await eurltoken.getGaslessBasefee(), 500, "Failed case40");
        await truffleAssert.fails(eurltoken.payGaslessBasefee(alice, bob, {from: forwarder}));
        console.log("case40 passed");

        //case 41 - new request type registration
        truffleAssert.eventEmitted(await forwarder.registerRequestType("ForwardRequest2", "uint256 validUntil)"), 'RequestTypeRegistered');
        console.log("case41 passed");

        //case 42 - minter tries to mint with feeRate

        //case 43 - minter tries to burn with feeRate
        
    });
});