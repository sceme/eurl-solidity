const {deployProxy, prepareUpgrade} = require('@openzeppelin/truffle-upgrades');
const truffleAssert = require('truffle-assertions');

const EURLTokenV2 = artifacts.require('EURLTokenTestV2');
const EURLToken = artifacts.require('EURLTokenTestV1');


contract('upgrades', (accounts) => {
  it("has a name", async function () {
    eurltoken = await deployProxy(EURLToken, { kind: 'uups' });
    await eurltoken.store(42);
    expect(await eurltoken.name()).to.equal("LUGH");
  });

  it("has a symbol", async function () {
    expect(await eurltoken.symbol()).to.equal("EURL");
  });
  it('deploys', async function () {
    console.log('V1 Deployed', eurltoken.address);
  });
  it('change ProxyAdmin Ownership', async () => {

    owner_dar = accounts[0];
    administrator = accounts[1];
    const masterMinter = accounts[2];
    minter = accounts[6];
    await eurltoken.setAdministrator(administrator, { from: owner_dar });

    await eurltoken.setMasterMinter(masterMinter, { from: owner_dar });
    await eurltoken.addMinter(minter, 10000,{ from: masterMinter });

    const owner = accounts[0];
    console.log("account owner  " +  owner );
    owner1= await eurltoken.owner();
    console.log("contract owner " + owner1);
  
    newOwner = accounts[7];

    await eurltoken.setOwner(newOwner,{ from: owner_dar });
    assert.equal(await eurltoken.hasRole('0x00', newOwner), true, "Failed change DEFAULT_ADMIN_ROLE");
    assert.equal(await eurltoken.hasRole('0x00', owner_dar), false, "Failed change DEFAULT_ADMIN_ROLE");

    await eurltoken.transferOwnership(newOwner);

    owner2= await eurltoken.owner();
    console.log("new owner " + owner2);

    //upgrade to new implementation
    //the upgrade is prepared but will be deployes by our gnosis Safe
    console.log("Preparing proposal...");
    eurltoken2 = await prepareUpgrade(eurltoken.address, EURLTokenV2);
    console.log('V2Upgrade proposal', eurltoken2);

  });
  it('upgrades', async function () {
    newOwner = accounts[7];
    eurltoken2_deploy = await eurltoken.upgradeTo(eurltoken2,{ from: newOwner });
    eurltoken2_deployed = await EURLTokenV2.at(await eurltoken.address);
    expect(await eurltoken2_deployed.name()).to.equal("LUGH");
    expect(await eurltoken2_deployed.symbol()).to.equal("EURL");
    await eurltoken2_deployed.increment();
    const value = await eurltoken2_deployed.retrieve();
    assert.equal(value.toString(), '43');
    console.log("value  " + value);
  });
  it('works', async () => {
    // case01 - Administrator try to upgrade
    await truffleAssert.fails(eurltoken.upgradeTo(eurltoken2,{ from: administrator }));
    // case02 - Minter try to upgrade
    await truffleAssert.fails(eurltoken.upgradeTo(eurltoken2,{ from: minter }));
    // case03 - Previous Owner try to upgrade
    await truffleAssert.fails(eurltoken.upgradeTo(eurltoken2,{ from: owner_dar }));
    // case04 - Administrator ty to transfer ownership
    await truffleAssert.fails(eurltoken.transferOwnership(newOwner,{ from: administrator}));
    // case05 - Minter ty to transfer ownership
    await truffleAssert.fails(eurltoken.transferOwnership(newOwner,{ from: minter}));
    // case06 - Previous Owner ty to transfer ownership
    await truffleAssert.fails(eurltoken.transferOwnership(newOwner,{ from: owner_dar}));
  });
});