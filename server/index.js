const express = require('express');
const app = express();

const {estimateGas} = require("../utils/gasPredict");
const {getLughTxLogs} = require("../utils/lughTxWatcher");

const eurlAddress = "0x88344216D4F259474b232C472bF5b40B2668D32f";
const etherscanApiKey = "";
const infuraWebSocket = 'wss://ropsten.infura.io/ws/v3/7f7902ffcef4413881034763efc04488';

app.get('/predictGas/:messagetype/:from', async (req,res) => {
    const Web3 = require('Web3');
    var web3 = new Web3(new Web3.providers.WebsocketProvider(infuraWebSocket));

    const address = req.params.from;
    const messageType = req.params.messagetype;

    const gas = await estimateGas(web3, eurlAddress, address, messageType);

    res.json(gas);
})

app.get('/getTx', async (req,res) => {
    const txLog = await getLughTxLogs(etherscanApiKey, eurlAddress, 1, 100);
    res.json(txLog);
})

app.listen(8080, () => {
    console.log("Serveur à l'écoute")
})