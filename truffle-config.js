const HDWalletProvider = require("@truffle/hdwallet-provider");
require('dotenv').config();

var mnemonic = process.env.MNEMONIC;
var infuraApiKey = process.env.INFURA_API_KEY;
var etherscanApiKey = process.env.ETHERSCAN_API_KEY;

module.exports = {
  networks: {
    ganache: {
      host: "127.0.0.1",     // Localhost (default: none)
      port: 7545,            // Standard Ethereum port (default: none)
      network_id: 5777,       // Any network (default: none)
      //gas: 0xfffffffffff,	// <-- Use this high gas value
      //gasPrice: 0x01,	// <-- Use this low gas price
    },

    sepolia: {
      provider: function () {
        return new HDWalletProvider(mnemonic, "wss://sepolia.infura.io/ws/v3/" + infuraApiKey);
      },
      network_id: 11155111,
      gas: 5500000, // Rinkeby has a lower block limit than mainnet
      networkCheckTimeout: 1000000,
      timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
      skipDryRun: true, // Skip dry run before migrations? (default: false for public nets )
    },

    goerli: {
      provider: function () {
        return new HDWalletProvider(mnemonic, "wss://goerli.infura.io/ws/v3/" + infuraApiKey);
      },
      network_id: 5,
      gas: 5500000, // Rinkeby has a lower block limit than mainnet
      confirmations: 2,
      networkCheckTimeout: 1000000,
      timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
      skipDryRun: true,
    },

    mainnet: {
      provider: function () {
        return new HDWalletProvider(mnemonic, "wss://mainnet.infura.io/ws/v3/" + infuraApiKey);
      },
      network_id: 1,
      gas: 5500000, // Rinkeby has a lower block limit than mainnet
      confirmations: 2,
      networkCheckTimeout: 1000000,
      timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
      skipDryRun: false,
    },

    matic: {
      provider: function () {
        return new HDWalletProvider(mnemonic, "https://polygon-mainnet.infura.io/v3/" + infuraApiKey);
      },
      network_id: '137',
    },
  },

  mocha: {
    timeout: 100000
  },

  compilers: {
    solc: {
      version: "^0.8.0",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      settings: {          // See the solidity docs for advice about optimization and evmVersion
        optimizer: {
          enabled: true,
          runs: 1
        },
        evmVersion: "istanbul"
      }
    }
  },

  plugins: ['truffle-plugin-verify'],
  api_keys: { etherscan: etherscanApiKey },
  contracts_directory: './Contracts',
};
