const sign = require('../utils/sign/metatx-sign');
const Web3 = require("web3");
const { abi } = require('../build/contracts/EURLTokenV2.json'); 

require('dotenv').config();

const infuraUrl = `https://goerli.infura.io/v3/${process.env.INFURA_API_KEY}`;
const web3 = new Web3(infuraUrl);
const tokenAddress = process.env.PROXY_CONTRACT;
const tokenContract = new web3.eth.Contract(abi, process.env.PROXY_CONTRACT);
const account = web3.eth.accounts.privateKeyToAccount(process.env.MNEMONIC);
web3.eth.accounts.wallet.add(account);
console.log(account);

var signPermit = async function(address, amount){
    const result = await sign.signERC2612Permit(web3.eth.currentProvider , tokenAddress, account, address, amount);
    return result;
}

var executePermit = async function(from, to, amount, permit){
    var tx = await tokenContract.permit(alice, bob, value, permit.deadline, permit.v, permit.r, permit.s, { from: account });
    console.log("Permit successfully executed @", tx.txHash);
}

module.exports = {signPermit, executePermit};